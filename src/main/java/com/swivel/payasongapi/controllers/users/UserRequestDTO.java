package com.swivel.payasongapi.controllers.users;

/**
 * UserRequestDTO class which extended by BaseRequestDTO class
 * User request validation methods implements in this class
 * @author Sampath
 * @since 2020-04-20
 */
public class UserRequestDTO extends BaseRequestDTO {

    private String name;
    private String email;
    private String genre;
    private String employeeNumber;
    private String jobTitle;
    public enum types {artist, operator };
    private String type;

    /**
     * User object input validation implement here.
     * Return true when all the required fields are available.
     * @return boolean true or false
     */
    @Override
    public boolean isRequiredAvailable() {
        if( isNonEmpty( type )) {
            if( isValidType( type )) {
               if (type.toLowerCase().equals("artist") ) {
                   return isNonEmpty(name) && isNonEmpty(email) && isNonEmpty(genre);
               } else if ( type.toLowerCase().equals("operator") ){
                   return isNonEmpty(name) && isNonEmpty(email) && isNonEmpty(employeeNumber) && isNonEmpty(jobTitle);
               }
               return true;
            }
            return false;
        }
        return false;
    }

    /**
     * Use to validate inout string is not empty
     * @param input
     * @return boolean true or false
     */
    @Override
    public boolean isNonEmpty(String input) {
        return (input != null && input.trim().length() > 0);
    }

    /**
     * Use this function to validate type
     * @param type
     * @return boolean true or false
     */
    public boolean isValidType(String type) {
        for (types c : types.values()) {
            if (c.name().equals(type)) {
                return true;
            }
        }
        return false;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getEmail(){
        return this.email;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public void setType(String type){
        this.type = type;
    }

    public String getType() {
        return type;
    }

}
