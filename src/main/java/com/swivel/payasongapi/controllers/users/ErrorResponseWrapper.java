package com.swivel.payasongapi.controllers.users;

/**
 * This class used to wrap error response of the request
 * @author Sampath
 * @since 2020-04-22
 */
public class ErrorResponseWrapper extends ResponseWrapper {
    private Integer errorStatus;
    private String displayMessage;

    public ErrorResponseWrapper(ErrorResponseStatusType errorResponseStatusType, UserRequestDTO userRequestDTO) {
        super(ResponseStatusType.ERROR, errorResponseStatusType.getMessage(), null);
        this.errorStatus = errorResponseStatusType.getCode();
        this.displayMessage = errorResponseStatusType.getMessage();
    }
 }
