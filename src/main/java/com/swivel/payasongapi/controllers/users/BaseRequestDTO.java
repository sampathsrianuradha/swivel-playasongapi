package com.swivel.payasongapi.controllers.users;
/**
 * Base request class dto provide base functions to the child classes
 * @author Sampath
 * @since 2020-04-20
 */
public class BaseRequestDTO {

    /**
     * Use this method to check availability of the failed
     * @return
     */
    public boolean isRequiredAvailable() {
        return true;
    }

    /**
     * Use this method to check given input is not empty.
     * If not empty it returns true and else return false
     * @param input
     * @return
     */
    public boolean isNonEmpty(String input) {
        return (input != null || input.trim().length() > 0);
    }

}
