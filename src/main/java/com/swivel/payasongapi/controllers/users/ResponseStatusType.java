package com.swivel.payasongapi.controllers.users;

/**
 * Enum for Response status types
 * @author Sampath
 * @since 2020-04-22
 */
public enum ResponseStatusType {

    SUCCESS(2001, "Successfully created the artist."),
    VALID_USER_TYPE(2002, "User type is valid."),
    ERROR(4001, "Required fields are missing.");

    private Integer code;
    private String message;

    ResponseStatusType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}