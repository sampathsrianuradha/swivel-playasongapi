package com.swivel.payasongapi.controllers.users;

/**
 * Enum for error response status types
 * @author Sampath
 * @since 2020-04-22
 */
public enum ErrorResponseStatusType {

    MISSING_REQUIRED_FIELDS( 400, "Required fields are missing."),
    INVALID_USER_TYPE( 400, "User type is invalid.");

    private Integer code;
    private String message;

    ErrorResponseStatusType(Integer code, String message){
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }
}
