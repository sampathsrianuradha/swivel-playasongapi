package com.swivel.payasongapi.controllers.users;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * This controller class used to wrap user endpoint.
 * User creation, update , delete etc.
 * @author Sampath
 * @since 2020-04-20
 */
@RestController
public class UserController {

    private String SUCCESS = "Successfully created the artist.";
    private String ERROR = "Post request was not validated!";
    /**
     * User creation POST method implement here.
     * @param userRequestDTO
     * @return
     */
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/users", produces = "application/json")
    public ResponseEntity<ResponseWrapper> createUser(@RequestBody UserRequestDTO userRequestDTO ){

        if (!userRequestDTO.isRequiredAvailable()) {
            ErrorResponseWrapper errorResponseWrapper = new ErrorResponseWrapper(ErrorResponseStatusType.MISSING_REQUIRED_FIELDS, userRequestDTO);
            return new ResponseEntity<>(errorResponseWrapper, HttpStatus.BAD_REQUEST);
        }
        ResponseWrapper responseWrapper = new ResponseWrapper(ResponseStatusType.SUCCESS, SUCCESS, userRequestDTO );
        return new ResponseEntity<>(responseWrapper, HttpStatus.OK);
    }
}
