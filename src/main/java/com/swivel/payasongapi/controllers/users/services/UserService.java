package com.swivel.payasongapi.controllers.users.services;

import com.swivel.payasongapi.controllers.users.UserRequestDTO;
import org.springframework.http.ResponseEntity;

public interface UserService {
    public ResponseEntity<Object> createUser(UserRequestDTO userRequestDto);
}
