package com.swivel.payasongapi.controllers.users;

import org.springframework.http.HttpStatus;

/**
 * This class used to wrap response of the request
 * @author Sampath
 * @since 2020-04-20
 */
public class ResponseWrapper<ResponseDTO> {
    private ResponseStatusType status;
    private String message;
    private ResponseDTO data;

    /**
     * Constructor method
     * @param status
     * @param message
     * @param data
     */
    public ResponseWrapper(ResponseStatusType status, String message, ResponseDTO data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public ResponseStatusType getStatus() {
        return status;
    }

    public void setStatus(ResponseStatusType status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseDTO getData() {
        return data;
    }

    public void setData(ResponseDTO data) {
        this.data = data;
    }
}
