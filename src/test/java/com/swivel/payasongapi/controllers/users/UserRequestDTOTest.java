package com.swivel.payasongapi.controllers.users;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * This class implements unit testing of UserRequestDTO class
 * @author Sampath
 * @since 2020-04-20
 */
class UserRequestDTOTest {

    /**
     * Testing isNonEmpty method of UserRequestDTO when given value is empty
     * Expected result is false
     */
    @Test
    void Should_ReturnFalse_When_GivenFieldValueIsEmpty(){
        UserRequestDTO userRequestDTO = new UserRequestDTO();
        String inputValue = "";
        Boolean result = userRequestDTO.isNonEmpty(inputValue);
        Assertions.assertFalse(result);
    }

    /**
     * Testing isNonEmpty method of UserRequestDTO when given value has space
     * Expected result is false
     */
    @Test
    void Should_ReturnFalse_When_GivenOneOfFieldValueHasSpaceOnly(){
        UserRequestDTO userRequestDTO = new UserRequestDTO();
        String inputValue = " ";
        Boolean result = userRequestDTO.isNonEmpty(inputValue);
        Assertions.assertFalse(result);
    }

    /**
     * Testing inNon Empty method of UserRequestDTO when given fields are not empty
     * Expected result is true
     */
    @Test
    void Should_ReturnTrue_When_GivenFieldsValuesAreNotEmpty(){
        UserRequestDTO userRequestDTO = new UserRequestDTO();
        String inputValue = "test";
        Boolean result = userRequestDTO.isNonEmpty(inputValue);
        Assertions.assertTrue(result);
    }

    /**
     * Testing user type input when it is not available
     * Expected result is false
     */
    @Test
    void Should_ReturnFalse_When_UserTypeIsNotAvailable() {
        UserRequestDTO userRequestDTO = new UserRequestDTO();
        userRequestDTO.setName("Pabilo Albo");
        userRequestDTO.setEmail("pablo@gmail.com");
        userRequestDTO.setGenre("pop");
        userRequestDTO.setEmployeeNumber("123");
        userRequestDTO.setJobTitle("accountant");
        userRequestDTO.setType("");
        Boolean result = userRequestDTO.isRequiredAvailable();
        Assertions.assertFalse(result);
    }

    /**
     * Testing user type input has only space, isRequiredAvailable return false.
     * Expected result is false
     */
    @Test
    void Should_ReturnFalse_When_UserTypeHasSpaceOnly() {
        UserRequestDTO userRequestDTO = new UserRequestDTO();
        userRequestDTO.setName("Pabilo Albo");
        userRequestDTO.setEmail("pablo@gmail.com");
        userRequestDTO.setGenre("pop");
        userRequestDTO.setType(" ");
        Boolean result = userRequestDTO.isRequiredAvailable();
        Assertions.assertFalse(result);
    }

    /**
     * Testing given user type, when it is available but not valid, isRequiredAvailable return false.
     * Expected result is false
     */
    @Test
    void Should_ReturnFalse_When_UserTypeIsNotValid() {
        UserRequestDTO userRequestDTO = new UserRequestDTO();
        userRequestDTO.setName("Pabilo Albo");
        userRequestDTO.setEmail("pablo@gmail.com");
        userRequestDTO.setGenre("pop");
        userRequestDTO.setEmployeeNumber("123");
        userRequestDTO.setJobTitle("accountant");
        userRequestDTO.setType("test");
        Boolean result = userRequestDTO.isRequiredAvailable();
        Assertions.assertFalse(result);
    }

    /**
     * Testing isRequiredAvailable method when at least one required field is empty.
     * Expected result is false
     */
    @Test
    void Should_ReturnFalse_When_AtLeastOneRequiredFieldIsNotAvailable() {
        UserRequestDTO userRequestDTO = new UserRequestDTO();
        userRequestDTO.setName("Pabilo Albo");
        userRequestDTO.setEmail("pablo@gmail.com");
        userRequestDTO.setGenre("");
        userRequestDTO.setType("artist");
        Boolean result = userRequestDTO.isRequiredAvailable();
        Assertions.assertFalse(result);
    }

    /**
     * Testing isRequiredAvailable method when all the required fields are not empty
     * Expected result is true.
     */
    @Test
    void Should_ReturnTrue_When_AllTheRequiredFieldsAreNotEmpty() {
        UserRequestDTO userRequestDTO = new UserRequestDTO();
        userRequestDTO.setName("Pabilo Albo");
        userRequestDTO.setEmail("pablo@gmail.com");
        userRequestDTO.setGenre("pop");
        userRequestDTO.setType("artist");
        Boolean result = userRequestDTO.isRequiredAvailable();
        Assertions.assertTrue(result);
    }

    /**
     * Testing isRequiredAvailable method when at least one required field is not available for operator user type
     * Expected result is false
     */
    @Test
    void Should_ReturnFalse_When_AtLeastOneRequiredFieldIsNotAvailableForOperator() {
        UserRequestDTO userRequestDTO = new UserRequestDTO();
        userRequestDTO.setName("Simon");
        userRequestDTO.setEmail("simon@playasong.com");
        userRequestDTO.setEmployeeNumber("123");
        userRequestDTO.setJobTitle("");
        userRequestDTO.setType("operator");
        Boolean result = userRequestDTO.isRequiredAvailable();
        Assertions.assertFalse(result);
    }
    /**
     * Testing isRequiredAvailable method when all the required fields are available for operator user type
     * Expected result is true
     */
    @Test
    void Should_ReturnTrue_When_AllTheRequiredFieldsAreNotEmptyForOperator() {
        UserRequestDTO userRequestDTO = new UserRequestDTO();
        userRequestDTO.setName("Simon");
        userRequestDTO.setEmail("simon@playasong.com");
        userRequestDTO.setEmployeeNumber("123");
        userRequestDTO.setJobTitle("accountant");
        userRequestDTO.setType("operator");
        Boolean result = userRequestDTO.isRequiredAvailable();
        Assertions.assertTrue(result);
    }
}