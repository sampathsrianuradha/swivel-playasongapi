package com.swivel.payasongapi.controllers.users;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class implements unit testing of UserController class
 * @author Sampath
 * @since 2020-04-22
 */
class UserControllerTest {

    private MockMvc mockMvc;

    /**
     * Test for validate return json of user artist creations
     * @throws Exception
     */
    @Test
    void Should_ReturnSuccessJson_When_PostedDetailsValidForArtist() throws Exception {
        /* TODO ------
            Pass post data
            Validate data
         */
        UserRequestDTO userRequestDTO = new UserRequestDTO();
        mockMvc.perform(MockMvcRequestBuilders.post("/users")
                .accept(MediaType.APPLICATION_JSON_VALUE));

    }

    /**
     * Test for validate return json of user operator creation
     */
    @Test
    void Should_ReturnSuccessJson_When_PostedDetailsValidForOperator(){

    }

    /**
     * Test for invalid user type
     */
    @Test
    void Should_ReturnErrorJson_When_UserTypeIsInValid(){

    }
}